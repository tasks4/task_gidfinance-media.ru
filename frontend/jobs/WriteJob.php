<?php

namespace frontend\jobs;

use yii\base\Object;

class WriteJob extends Object implements \yii\queue\Job
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $categoryName;

    /**
     * @var string
     */
    public $file;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     */
    public function execute($queue)
    {
        echo $this->categoryName . "\n";
        sleep(2);

        $response =
            sprintf(
                "lead_id: %s | lead_category: %s | current_datetime: %s \n",
                $this->id,
                $this->categoryName,
                date('Y-m-d H:i:s', time())
            );

        file_put_contents($this->file, $response, FILE_APPEND);
    }
}
